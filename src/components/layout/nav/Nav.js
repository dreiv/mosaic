import { Hidden } from '@material-ui/core';
import React from 'react';
import MainNav from './main-nav';
import MobileNav from './mobile-nav';

function Nav() {
	return (
		<nav>
			<Hidden mdUp>
				<MobileNav />
			</Hidden>

			<Hidden xsDown>
				<MainNav />
			</Hidden>
		</nav>
	)
}
Nav.whyDidYouRender = true

export { Nav }
