import { Drawer } from '@material-ui/core';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useRootState } from 'components/layout/root';
import React from 'react';
import NavContent from '../nav-content';
import { classList } from 'utils';
import useStyles from './MainNav.styles';

function MainNav({ className }) {
	const { nav: { mainOpen } = {} } = useRootState()
	const isMdUp = useMediaQuery(({ breakpoints: { up } }) => up('md'))
	const classes = useStyles()
	const open = mainOpen && isMdUp

	const iffyClassNames = classList(
		open && classes.drawerOpen,
		!open && classes.drawerClose
	)

	const classNames = classList(
		className,
		classes.drawer,
		iffyClassNames
	)

	return (
		<Drawer
			variant="permanent"
			open={open}
			className={classNames}
			classes={{
				paper: iffyClassNames,
			}}
		>
			<NavContent isCollapsed={!open} />
		</Drawer>
	)
}
MainNav.whyDidYouRender = true

export default MainNav
