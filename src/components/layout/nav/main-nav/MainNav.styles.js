import { makeStyles } from '@material-ui/core/styles';
import { drawerWidth } from 'components/layout/properties.json';

export default makeStyles(({
	transitions,
	spacing
}) => ({
	drawer: {
		flexShrink: 0,
		whiteSpace: 'nowrap',
		overflowX: 'hidden',
	},
	drawerOpen: {
		width: drawerWidth,
		transition: transitions.create('width', {
			easing: transitions.easing.sharp,
			duration: transitions.duration.enteringScreen,
		}),
	},
	drawerClose: {
		transition: transitions.create('width', {
			easing: transitions.easing.sharp,
			duration: transitions.duration.leavingScreen,
		}),
		width: spacing(9) + 1,
	}
}))
