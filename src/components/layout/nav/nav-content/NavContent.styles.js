import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(({ mixins }) => ({
	header: {
		display: 'flex',
		alignItems: 'center',
		...mixins.toolbar
	},
	logo: {
		flexGrow: 1
	}
}))
