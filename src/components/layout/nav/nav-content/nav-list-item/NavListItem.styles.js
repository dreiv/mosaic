import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(({ palette }) => ({
	item: {
		'&.active': { // injected by react router
			color: palette.primary.main,
			'& .MuiListItemIcon-root': {
				color: palette.primary.main
			}
		}
	},
	collapsedItem: {
		flexDirection: 'column'
	},
	collapsedIcon: {
		minWidth: 0
	}
}))
