import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import React, { forwardRef } from 'react';
import { NavLink } from 'react-router-dom';
import { classList } from 'utils';
import useStyles from './NavListItem.styles';

const Link = forwardRef((props, ref) =>
	<NavLink exact role="button" {...props} />
)

function NavListItem({
	isCollapsed,
	Icon,
	text,
	...rest
}) {
	const classes = useStyles()

	const itemClasses = classList(
		classes.item,
		isCollapsed && classes.collapsedItem
	)
	const iconClasses = classList(isCollapsed && classes.collapsedIcon)

	return (
		<ListItem
			button
			component={Link}
			className={itemClasses}
			alignItems='center'
			{...rest}
		>
			<ListItemIcon className={iconClasses}>
				<Icon />
			</ListItemIcon>

			<ListItemText primary={text} />
		</ListItem>
	)
}
NavListItem.whyDidYouRender = true

export default NavListItem
