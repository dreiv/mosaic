import React, { memo } from 'react'
import { Typography } from '@material-ui/core'

function Logo({
	className,
	...rest
}) {
	return (
		<Typography
			className={className}
			noWrap
			{...rest}
		>
			Logo
		</Typography>
	)
}
Logo.whyDidYouRender = true

export default memo(Logo)