import {
	Box,
	Divider,
	IconButton,
	List
} from '@material-ui/core';
import { ChevronLeft, Bookmarks, Dashboard, Settings } from '@material-ui/icons';
import { useRootDispatch, rootTypes } from 'components/layout/root';
import React, { memo } from 'react';
import NavListItem from './nav-list-item'
import Logo from './logo'
import useStyles from './NavContent.styles';


function NavContent({
	isCollapsed,
	onItemClick = () => { }
}) {
	const dispatch = useRootDispatch()
	const classes = useStyles()

	return (
		<>
			<Box className={classes.header} pl={2} pr={1}>
				<Logo className={classes.logo} />
				<IconButton onClick={() => dispatch({ type: rootTypes.closeNav })}>
					<ChevronLeft />
				</IconButton>
			</Box >
			<Divider />

			<List>
				<NavListItem
					to='/'
					Icon={Dashboard}
					text='Posts'
					isCollapsed={isCollapsed}
					onClick={onItemClick}
				/>

				<NavListItem
					to='/saved'
					Icon={Bookmarks}
					text='Saved'
					isCollapsed={isCollapsed}
					onClick={onItemClick}
				/>

				<NavListItem
					to='/settings'
					Icon={Settings}
					text='Prefs'
					isCollapsed={isCollapsed}
					onClick={onItemClick}
				/>
			</List>
		</>
	)
}
NavContent.whyDidYouRender = true

export default memo(NavContent)