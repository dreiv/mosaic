import { makeStyles } from '@material-ui/core/styles';
import { drawerWidth } from 'components/layout/properties.json';

export default makeStyles(() => ({
	drawerPaper: {
		width: drawerWidth,
	}
}))
