import { SwipeableDrawer } from '@material-ui/core';
import { useRootState, useRootDispatch, rootTypes } from 'components/layout/root';
import React, { useCallback } from 'react';
import NavContent from '../nav-content';
import useStyles from './MobileNav.styles';

function MobileNav() {
	const { nav: { mobileOpen } = {} } = useRootState()
	const dispatch = useRootDispatch()
	const classes = useStyles()

	const handleClose = useCallback(
		() => dispatch({ type: rootTypes.closeNav }),
		[dispatch]
	)

	return (
		<SwipeableDrawer
			variant="temporary"
			open={mobileOpen}
			onOpen={() => dispatch({ type: rootTypes.openNav })}
			onClose={handleClose}
			classes={{
				paper: classes.drawerPaper,
			}}
			ModalProps={{
				keepMounted: true, // Better open performance on mobile.
			}}
		>
			<NavContent onItemClick={handleClose} />
		</SwipeableDrawer>
	)
}
MobileNav.whyDidYouRender = true

export default MobileNav