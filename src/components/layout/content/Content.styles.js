import { makeStyles } from '@material-ui/styles'

export default makeStyles(({ mixins }) => ({
	content: {
		flexGrow: 1,
		maxWidth: '60rem'
	},
	toolbar: mixins.toolbar
}))
