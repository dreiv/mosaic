import { Box } from '@material-ui/core';
import React from 'react';
import useStyles from './Content.styles';

function Content({ children }) {
	const classes = useStyles()

	return (
		<Box
			component='main'
			className={classes.content}
			mx='auto'
		>
			<Box className={classes.toolbar} />

			{children}
		</Box>
	)
}
Content.whyDidYouRender = true

export { Content }
