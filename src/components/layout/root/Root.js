import React from 'react'
import { RootProvider } from './root-context'
import { Header, Nav, Content } from 'components/layout'

function Root({ children }) {
	return (
		<RootProvider>
			<Header />
			<Nav />
			<Content>
				{children}
			</Content>
		</RootProvider>
	)
}
Root.whyDidYouRender = true

export default Root
