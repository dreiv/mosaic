import { useState, useEffect } from 'react'
import { useRootDispatch, rootTypes } from 'components/layout/root';

function useHeader(initialConfig = {}) {
	const [config, setConfig] = useState(initialConfig)
	const dispatch = useRootDispatch()

	useEffect(() => {
		dispatch({
			type: rootTypes.headerConfig,
			config
		})

		return () => dispatch({ type: rootTypes.headerReset })
	}, [config, dispatch])

	return setConfig
}

export { useHeader }