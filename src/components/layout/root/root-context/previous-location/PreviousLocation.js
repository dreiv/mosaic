import { withRouter } from "react-router";
import { useRootDispatch, rootTypes } from 'components/layout/root';
import { memo, useEffect } from 'react';
import { usePrevious } from 'utils';

function PreviousLocation({ location }) {
	const dispatch = useRootDispatch()
	const prevLocation = usePrevious(location) || {}

	useEffect(() => {
		if (location.key !== prevLocation.key) {
			const previousLocation = prevLocation.pathname
			dispatch({ type: rootTypes.previousLocation, previousLocation })
		}
	}, [dispatch, location, prevLocation])

	return null
}
PreviousLocation.whyDidYouRender = true

export default withRouter(memo(PreviousLocation))
