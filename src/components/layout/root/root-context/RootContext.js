import React, { createContext, useContext, useReducer } from 'react';
import { getLocalStorageItem } from 'utils';
import { closeNav, nonPhone, onPhone, openNav } from './actions/nav';
import { darkTheme, lightTheme } from './actions/theme';
import { previousLocation, headerConfig, headerReset } from './actions/header';
import { loadPosts, savePost, unSavePost } from './actions/posts';
import { localStorageKeys } from 'components/layout/properties.json';
import PreviousLocation from './previous-location';
import Theme from './theme';

const RootStateContext = createContext()
const RootDispatchContext = createContext()

function rootReducer(state, action) {
	switch (action.type) {
		case rootTypes.darkTheme:
			return darkTheme(state)
		case rootTypes.lightTheme:
			return lightTheme(state)

		case rootTypes.openNav:
			return openNav(state)
		case rootTypes.closeNav:
			return closeNav(state)
		case rootTypes.onPhone:
			return onPhone(state)
		case rootTypes.nonPhone:
			return nonPhone(state)

		case rootTypes.previousLocation:
			return previousLocation(state, action.previousLocation)
		case rootTypes.headerConfig:
			return headerConfig(state, action.config)
		case rootTypes.headerReset:
			return headerReset(state)

		case rootTypes.loadPosts:
			return loadPosts(state)
		case rootTypes.savePost:
			return savePost(state, action.postId)
		case rootTypes.unSavePost:
			return unSavePost(state, action.postId)

		default: {
			throw new Error(`Unhandled action type: ${action.type}`)
		}
	}
}
const rootTypes = {
	darkTheme: 'darkTheme',
	lightTheme: 'lightTheme',

	openNav: 'openNav',
	closeNav: 'closeNav',
	onPhone: 'onPhone',
	nonPhone: 'nonPhone',

	previousLocation: 'previousLocation',
	headerConfig: 'headerConfig',
	headerReset: 'headerReset',

	loadPosts: 'loadPosts',
	savePost: 'savePost',
	unSavePost: 'unSavePost',
}

const initialState = {
	isDarkTheme: getLocalStorageItem(localStorageKeys.darkTheme, false),
	nav: {
		mobileOpen: false,
		mainOpen: getLocalStorageItem(localStorageKeys.mainNavOpen, true)
	},
	header: {},
	posts: {
		loaded: [],
		offset: 0,
		isFinished: false,
		savedIds: getLocalStorageItem(localStorageKeys.savedPostIds, [])
	}
}

function RootProvider({ children }) {
	const [state, dispatch] = useReducer(rootReducer, initialState)

	return (
		<RootStateContext.Provider value={state}>
			<RootDispatchContext.Provider value={dispatch}>
				<PreviousLocation />
				<Theme>
					{children}
				</Theme>
			</RootDispatchContext.Provider>
		</RootStateContext.Provider>
	)
}
RootProvider.whyDidYouRender = true

function useRootState() {
	const context = useContext(RootStateContext)

	if (context === undefined) {
		throw new Error('useRootState must be used within a RootProvider')
	}
	return context
}

function useRootDispatch() {
	const context = useContext(RootDispatchContext)

	if (context === undefined) {
		throw new Error('useRootDispatch must be used within a RootProvider')
	}
	return context
}

export {
	RootProvider,
	useRootState,
	useRootDispatch,
	rootTypes
}
