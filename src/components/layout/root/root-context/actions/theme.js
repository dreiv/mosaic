import { setLocalStorageItem } from 'utils'
import { localStorageKeys } from 'components/layout/properties.json';

function darkTheme(state) {
	const isDarkTheme = true
	setLocalStorageItem(localStorageKeys.darkTheme, isDarkTheme)

	return { ...state, isDarkTheme }
}

function lightTheme(state) {
	const isDarkTheme = false
	setLocalStorageItem(localStorageKeys.darkTheme, isDarkTheme)

	return { ...state, isDarkTheme }
}

export { darkTheme, lightTheme }
