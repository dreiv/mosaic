import { setLocalStorageItem } from 'utils'
import { localStorageKeys } from 'components/layout/properties.json';

const handleTargetNav = (isPhone, value) => {
	if (isPhone) {
		return { mobileOpen: value }
	}
	setLocalStorageItem(localStorageKeys.mainNavOpen, value)

	return { mainOpen: value }
}

function openNav(state) {
	const { nav, nav: { isPhone } } = state

	return {
		...state,
		nav: {
			...nav,
			open: true,
			...handleTargetNav(isPhone, true)
		}
	}
}

function closeNav(state) {
	const { nav, nav: { isPhone } } = state

	return {
		...state,
		nav: {
			...nav,
			open: false,
			...handleTargetNav(isPhone, false)
		}
	}
}

function onPhone(state) {
	const { nav, nav: { mobileOpen } } = state

	return {
		...state,
		nav: {
			...nav,
			isPhone: true,
			open: mobileOpen
		}
	}
}

function nonPhone(state) {
	const { nav, nav: { mainOpen } } = state

	return {
		...state,
		nav: {
			...nav,
			isPhone: false,
			open: mainOpen
		}
	}
}

export { openNav, closeNav, onPhone, nonPhone }
