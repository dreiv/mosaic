import { Menu } from 'components/layout/header/navigation'

const defaultHeader = {
	Navigation: Menu,
	title: '',
	Actions: []
}

function previousLocation(state, previousLocation) {
	const { header } = state

	return {
		...state,
		header: {
			...header,
			previousLocation
		}
	}
}

function headerConfig(state, config) {
	const { header } = state

	return {
		...state,
		header: {
			...header,
			...config
		}
	}
}

function headerReset(state) {
	const { header } = state

	return {
		...state,
		header: {
			...header,
			...defaultHeader
		}
	}
}

export { previousLocation, headerConfig, headerReset }