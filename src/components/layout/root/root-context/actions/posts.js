import data from 'mock_data/posts.json'
import { setLocalStorageItem } from 'utils'
import { localStorageKeys } from 'components/layout/properties.json';
import { shuffle } from 'utils'

const INCREMENT = 10
const shuffledPosts = shuffle(data)

function loadPosts(state) {
	const { posts, posts: { loaded, offset } } = state
	const isFinished = data.length === loaded.length
	const update = {}

	if (!isFinished) {
		const intervalEnd = offset + INCREMENT
		update.loaded = [
			...loaded,
			...shuffledPosts.slice(offset, intervalEnd)
		]
		update.offset = intervalEnd
	}

	return {
		...state,
		posts: {
			...posts,
			...update,
			isFinished
		}
	}
}

function savePost(state, postId) {
	const { posts } = state
	const savedIds = [...posts.savedIds, postId]

	setLocalStorageItem(localStorageKeys.savedPostIds, savedIds)

	return {
		...state,
		posts: {
			...posts,
			savedIds
		}
	}
}

function unSavePost(state, postId) {
	const { posts, posts: { savedIds } } = state
	const postIndex = savedIds.indexOf(postId)
	const updatedIds = [...posts.savedIds]
	updatedIds.splice(postIndex, 1)

	setLocalStorageItem(localStorageKeys.savedPostIds, updatedIds)

	return {
		...state,
		posts: {
			...posts,
			savedIds: updatedIds
		}
	}
}

export { loadPosts, savePost, unSavePost }