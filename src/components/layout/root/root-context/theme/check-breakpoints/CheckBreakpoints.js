import useMediaQuery from '@material-ui/core/useMediaQuery';
import { useRootDispatch, rootTypes } from 'components/layout/root';
import { memo } from 'react';

function CheckBreakpoints() {
	const dispatch = useRootDispatch()
	const isPhone = useMediaQuery(({ breakpoints: { down } }) => down('sm'))

	dispatch({
		type: isPhone
			? rootTypes.onPhone
			: rootTypes.nonPhone
	})

	return null
}
CheckBreakpoints.whyDidYouRender = true

export default memo(CheckBreakpoints)
