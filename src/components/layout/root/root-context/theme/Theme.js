import React, { memo, useMemo } from 'react'
import { createMuiTheme } from '@material-ui/core/styles'
import { CssBaseline } from '@material-ui/core'
import { ThemeProvider } from '@material-ui/styles'

import { useRootState } from 'components/layout/root';
import CheckBreakpoints from './check-breakpoints';

function Theme({ children }) {
	const { isDarkTheme } = useRootState()
	const theme = useMemo(() => createMuiTheme({
		palette: {
			type: isDarkTheme ? 'dark' : 'light'
		}
	}), [isDarkTheme])

	return (
		<ThemeProvider theme={theme}>
			<CheckBreakpoints />
			<CssBaseline />

			{children}
		</ThemeProvider>
	)
}
Theme.whyDidYouRender = true

export default memo(Theme)