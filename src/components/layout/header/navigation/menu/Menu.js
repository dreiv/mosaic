import { IconButton } from '@material-ui/core';
import { Menu as MenuIcon } from '@material-ui/icons';
import { useRootState, useRootDispatch, rootTypes } from 'components/layout/root';
import React, { memo } from 'react';
import { classList } from 'utils';
import useStyles from './Menu.styles';

function Menu({
	className,
	...rest
}) {
	const { nav = {} } = useRootState()
	const dispatch = useRootDispatch()
	const classes = useStyles()

	const classNames = classList(
		className,
		nav.open && classes.hide
	)

	return (
		<IconButton
			color="inherit"
			edge="start"
			onClick={() => dispatch({ type: rootTypes.openNav })}
			className={classNames}
			aria-label="menu"
			{...rest}
		>
			<MenuIcon />
		</IconButton>
	)
}
Menu.whyDidYouRender = true

export default memo(Menu)
