import { makeStyles } from '@material-ui/core/styles';

export default makeStyles(({
	breakpoints
}) => ({
	hide: {
		[breakpoints.up('md')]: {
			display: 'none'
		}
	}
}))
