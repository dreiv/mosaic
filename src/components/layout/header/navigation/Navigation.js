import React, { memo } from 'react'
import { useRootState } from 'components/layout/root'
import Menu from './menu'

function Navigation(props) {
	const { header: { Navigation = Menu } = {} } = useRootState()

	return <Navigation {...props} />
}
Navigation.whyDidYouRender = true

export default memo(Navigation)