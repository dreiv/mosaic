import { IconButton } from '@material-ui/core';
import { ArrowBack } from '@material-ui/icons';
import React, { memo } from 'react';
import { Link } from 'react-router-dom';
import { useRootState } from 'components/layout/root';

function Back({ className }) {
	const { header: { previousLocation = '/' } = {} } = useRootState()

	return (
		<IconButton
			color="inherit"
			edge="start"
			component={Link}
			className={className}
			aria-label="back"
			to={previousLocation}
		>
			<ArrowBack />
		</IconButton>
	)
}
Back.whyDidYouRender = true

export default memo(Back)
