import { makeStyles } from '@material-ui/core/styles';
import { drawerWidth } from 'components/layout/properties.json';

export default makeStyles(({
	breakpoints,
	spacing,
	transitions,
	zIndex,
}) => ({
	appBar: {
		zIndex: zIndex.drawer + 1,
		transition: transitions.create(['width', 'margin'], {
			easing: transitions.easing.sharp,
			duration: transitions.duration.leavingScreen,
		}),
	},
	appBarShift: {
		[breakpoints.up('md')]: {
			width: `calc(100% - ${drawerWidth}px)`,
		},
		transition: transitions.create(['width', 'margin'], {
			easing: transitions.easing.sharp,
			duration: transitions.duration.enteringScreen,
		}),
	},
	navButton: {
		marginRight: spacing(),
		[breakpoints.up('sm')]: {
			marginRight: spacing(3),
		}
	},
	title: {
		flexGrow: 1,
	}
}))
