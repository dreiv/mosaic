import React, { memo } from 'react'
import { useRootState } from 'components/layout/root'
import { Typography } from '@material-ui/core'

function Title({
	className
}) {
	const { header: { title = '' } = {} } = useRootState()

	return (
		<Typography
			variant="h6"
			className={className}
			noWrap
		>
			{title}
		</Typography>
	)
}
Title.whyDidYouRender = true

export default memo(Title)
