import { AppBar, Toolbar } from '@material-ui/core';
import { useRootState } from 'components/layout/root';
import React from 'react';
import { classList } from 'utils';
import useStyles from './Header.styles';
import Navigation from './navigation';
import Title from './title';

function Header({
	className
}) {
	const { nav = {} } = useRootState()
	const classes = useStyles()

	const classNames = classList(
		className,
		classes.appBar,
		nav.open && classes.appBarShift
	)

	return (
		<AppBar
			className={classNames}
			position="fixed"
		>
			<Toolbar>
				<Navigation className={classes.navButton} />
				<Title className={classes.title} />
			</Toolbar>
		</AppBar >
	)
}
Header.whyDidYouRender = true

export { Header }