import { useTheme } from '@material-ui/styles';
import React from 'react';

const imgUrl = (id, width, height) =>
	`https://picsum.photos/id/${id}/${width}/${height}`

export default function Image({
	id,
	alt,
	...rest
}) {
	const { breakpoints } = useTheme();
	const dimensions = [
		{
			width: 480,
			height: 270,
			breakpoint: `${breakpoints.width('sm')}w`,
			size: `${breakpoints.up('sm')} 27.5rem`
		},
		{
			width: 800,
			height: 450,
			breakpoint: `${breakpoints.width('md')}w`,
			size: `${breakpoints.up('md')} 37.5rem`
		},
		{
			width: 1024,
			height: 576,
			breakpoint: `${breakpoints.width('lg')}w`,
			size: '64rem'
		}
	]

	const [imgUrls, sources, sizes] = dimensions.reduce((
		[urls, sources, sizes],
		{ width, height, breakpoint, size }
	) => {
		const url = imgUrl(id, width, height)
		const source = `${url} ${breakpoint}`

		return [
			[...urls, url],
			[...sources, source],
			[...sizes, size]
		]
	}, [[], [], []])

	return (
		<img
			srcSet={sources.join(', ')}
			sizes={sizes.join(' ')}
			src={imgUrls[1]}
			alt={alt}
			loading="lazy"
			{...rest}
		/>
	)
}
