import { List, ListItem, ListItemIcon, ListItemText, Switch } from '@material-ui/core';
import { Brightness4 as DarkThemeIcon } from '@material-ui/icons';
import { useRootState, useRootDispatch, rootTypes } from 'components/layout/root';
import React from 'react';
import Back from 'components/layout/header/navigation/back';
import { useHeader } from 'components/layout/root/root-context/hooks';

const headerConfig = {
	Navigation: Back,
	title: 'Settings'
}

function Settings() {
	const { isDarkTheme } = useRootState()
	const dispatch = useRootDispatch()
	useHeader(headerConfig)

	const handleDarkThemeToggle = () => {
		dispatch({
			type: isDarkTheme
				? rootTypes.lightTheme
				: rootTypes.darkTheme
		})
	}

	return (
		<List>
			<ListItem
				button
				onClick={handleDarkThemeToggle}
			>
				<ListItemIcon>
					<DarkThemeIcon />
				</ListItemIcon>

				<ListItemText
					primary="Dark theme"
					secondary="Enable dark theme throughout the app"
				/>
				<Switch checked={isDarkTheme} />
			</ListItem>
		</List>
	)
}
Settings.whyDidYouRender = true

export default Settings
