import { Typography } from '@material-ui/core';
import React from 'react';
import { useHeader } from 'components/layout/root/root-context/hooks'
import Back from 'components/layout/header/navigation/back'

const headerConfig = {
	Navigation: Back,
	title: 'Creators'
}

function Creators() {
	useHeader(headerConfig)

	return (
		<Typography variant="h2">
			To be implemented
		</Typography>
	)
}
Creators.whyDidYouRender = true

export default Creators
