import Root from 'components/layout'
import React, { lazy, Suspense } from 'react'
import { Route, Switch } from 'react-router-dom'
import NotFound from './not-found'
import Post from './post'
import Posts from './posts'

const Saved = lazy(() => import('./saved'))
const Settings = lazy(() => import('./settings'))
const Creators = lazy(() => import('./creators'))

function Routes() {
  return (
    <Root>
      <Suspense fallback="Loading...">
        <Switch>
          <Route exact path="/" component={Posts} />
          <Route exact path="/post/:postId" component={Post} />
          <Route path="/saved" component={Saved} />
          <Route path="/creators" component={Creators} />
          <Route path="/settings" component={Settings} />
          <Route component={NotFound} />
        </Switch>
      </Suspense>
    </Root>
  )
}
Routes.whyDidYouRender = true

export default Routes
