import { Typography } from '@material-ui/core';
import React from 'react';
import { useHeader } from 'components/layout/root/root-context/hooks';

const headerConfig = {
	title: 'Saved'
}

function Saved() {
	useHeader(headerConfig)

	return (
		<Typography variant="h2">
			Saved
		</Typography>
	)
}
Saved.whyDidYouRender = true

export default Saved
