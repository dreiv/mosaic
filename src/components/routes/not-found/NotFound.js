import { Typography } from '@material-ui/core';
import React from 'react';
import { useHeader } from 'components/layout/root/root-context/hooks'
import Back from 'components/layout/header/navigation/back'

const headerConfig = {
	Navigation: Back,
	title: 'Not Found'
}

function NotFound() {
	useHeader(headerConfig)

	return (
		<Typography variant="h2">
			404
		</Typography>
	)
}
NotFound.whyDidYouRender = true

export default NotFound
