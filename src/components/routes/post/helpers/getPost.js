import posts from 'mock_data/posts.json'

function getPost(postId) {
  return posts.find(({ id }) => id === postId)
}

export { getPost }
