import { Container, Typography, Link } from '@material-ui/core'
import Back from 'components/layout/header/navigation/back'
import { useHeader } from 'components/layout/root/root-context/hooks'
import React, { useMemo } from 'react'
import Image from 'components/common/image'
import { Link as RouterLink } from 'react-router-dom'
import { getPost } from './helpers/getPost'
import useStyles from './Post.styles'

const headerConfig = title => ({
	Navigation: Back,
	title
})

function Post({
	match: {
		params: { postId }
	}
}) {
	const classes = useStyles()
	const { id, alt, title, author } = useMemo(() => getPost(postId), [postId])
	useHeader(headerConfig(title))

	return (
		<>
			<Image className={classes.image} id={id} alt={alt} />

			<Container>
				<Typography variant="h4" className={classes.title}>
					by{' '}
					<Link
						color="inherit"
						component={RouterLink}
						to={`/creators`}
					>
						{author}
					</Link>
				</Typography>

				<Typography variant="h4">
					more like this:
				</Typography>
			</Container>
		</>
	)
}
Post.whyDidYouRender = true

export default Post
