import { makeStyles } from '@material-ui/core/styles'

export default makeStyles(() => ({
	imageWrapper: {
		overflowX: 'auto'
	},
	image: {
		width: '100%'
	},
	title: {
		textAlign: 'end'
	}
}))
