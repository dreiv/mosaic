import { Box, Typography } from '@material-ui/core'
import { useHeader } from 'components/layout/root/root-context/hooks'
import React, { useLayoutEffect } from 'react'
import { useIntersect } from 'utils'
import { useRootState, useRootDispatch, rootTypes } from 'components/layout/root';
import useStyles from './Posts.styles'
import Tile from './tile'

const headerConfig = { title: 'Posts' }

function Posts() {
	useHeader(headerConfig)
	const [intersectRef, intersects] = useIntersect()
	const { posts: { loaded, isFinished } = {} } = useRootState()
	const dispatch = useRootDispatch()
	const classes = useStyles()

	useLayoutEffect(() => {
		if (!isFinished && intersects) {
			dispatch({ type: rootTypes.loadPosts })
		}
	}, [isFinished, intersects, dispatch])

	return (
		<Box mb={16}>
			<Box className={classes.grid} component="ul" mt={0}>
				{loaded.map(post => (
					<Tile
						key={post.id}
						className={classes.tile}
						post={post}
					/>
				))}
				{!isFinished && (
					<li className={classes.loadMore} ref={intersectRef} />
				)}
			</Box>
			{isFinished && (
				<Typography variant="h2">You reached the end :D</Typography>
			)}
		</Box>
	)
}
Posts.whyDidYouRender = true

export default Posts
