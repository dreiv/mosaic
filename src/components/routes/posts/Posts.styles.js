import { makeStyles } from '@material-ui/core/styles'

export default makeStyles(({ breakpoints }) => ({
	grid: {
		position: 'relative',
		display: 'grid',
		gridTemplateColumns: 'repeat(auto-fill, minmax(18rem, 1fr))',
		gridGap: '.5rem',
		gridAutoRows: 'minmax(18rem, auto)',
		gridAutoFlow: 'dense',
		padding: 0,
		listStyle: 'none'
	},
	tile: {
		[breakpoints.up('sm')]: {
			'&:nth-of-type(3n)': {
				gridColumn: 'span 2'
			}
		},
		'&:nth-of-type(5n)': {
			gridRow: 'span 2'
		}
	},
	loadMore: {
		position: 'absolute',
		height: '50vh',
		bottom: 0,
		width: '100%',
		pointerEvents: 'none'
	}
}))
