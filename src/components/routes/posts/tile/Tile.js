import { Box, Link } from '@material-ui/core'
import Image from 'components/common/image'
import React, { memo } from 'react'
import { Link as RouterLink } from 'react-router-dom'
import TileHeader from './tile-header'
import TileFooter from './tile-footer'
import { classList } from 'utils'
import useStyles from './Tile.styles'

function Tile({
	className,
	post: { id, alt, color, title, author }
}) {
	const classes = useStyles({ color })
	const classNames = classList(
		className,
		classes.tile,
		classes.tileBackground
	)

	return (
		<Box className={classNames} component="li">
			<Link component={RouterLink} to={`/post/${id}`}>
				<Image className={classes.image} id={id} alt={alt} />
			</Link>

			<TileHeader
				className={classes.visible}
				id={id}
				title={title}
			/>

			<TileFooter
				className={classes.visible}
				author={author}
			/>
		</Box>
	)
}
Tile.whyDidYouRender = true

export default memo(Tile)
