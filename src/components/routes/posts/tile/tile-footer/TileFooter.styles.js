import { makeStyles } from '@material-ui/core/styles'

export default makeStyles(({ spacing }) => ({
	footer: {
		pointerEvents: 'none',
		position: 'absolute',
		width: '100%',
		bottom: 0,
		background: `
			linear-gradient(to top, rgba(0,0,0,0.7) 0%,
			rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)`
	},
	link: {
		pointerEvents: 'initial',
		display: 'inline-flex',
		alignItems: 'center',
		'&:hover': {
			textDecoration: 'none',
			'& $author': {
				textDecoration: 'underline'
			}
		}
	},
	avatar: {
		marginRight: spacing(1),
		backgroundColor: `#fff4`,
		textTransform: 'uppercase'
	},
	author: { /* Used by link */ }
}))
