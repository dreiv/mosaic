import { Box, Link, Avatar } from '@material-ui/core'
import React, { useMemo } from 'react'
import { Link as RouterLink } from 'react-router-dom'
import { classList, getInitials } from 'utils'
import useStyles from './TileFooter.styles'

function TileFooter({ className, author }) {
	const classes = useStyles()
	const classNames = classList(
		className,
		classes.footer
	)

	const initials = useMemo(
		() => getInitials(author)
		, [author])

	return (
		<Box className={classNames} p={1}>
			<Link
				component={RouterLink}
				color="inherit"
				to="/creators"
				variant="subtitle1"
				className={classes.link}
			>
				<Avatar className={classes.avatar}>{initials}</Avatar>
				<Box className={classes.author}>{author}</Box>
			</Link>
		</Box>
	)
}
TileFooter.whyDidYouRender = true

export default TileFooter
