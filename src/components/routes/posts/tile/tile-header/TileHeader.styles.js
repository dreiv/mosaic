import { makeStyles } from '@material-ui/core/styles'

export default makeStyles(() => ({
	header: {
		display: 'flex',
		alignItems: 'center',
		position: 'absolute',
		top: 0,
		width: '100%',
		background: `
			linear-gradient(to bottom, rgba(0,0,0,0.7) 0%,
			rgba(0,0,0,0.3) 70%, rgba(0,0,0,0) 100%)`
	},
	link: {
		flex: 1
	},
	icon: {
		color: 'white'
	}
}))
