import { Box, IconButton, Link } from '@material-ui/core'
import { Share, Star, StarBorder } from '@material-ui/icons'
import {
	rootTypes,
	useRootDispatch,
	useRootState
} from 'components/layout/root'
import React from 'react'
import { Link as RouterLink } from 'react-router-dom'
import { classList } from 'utils'
import useStyles from './TileHeader.styles'

const MaybeSavedIcon = ({ isSaved }) => (isSaved ? <Star /> : <StarBorder />)

function TileHeader({ className, id, title }) {
	const { posts: { savedIds } = {} } = useRootState()
	const dispatch = useRootDispatch()
	const classes = useStyles()
	const isSaved = savedIds.includes(id)
	const classNames = classList(className, classes.header)

	const handleSave = () =>
		dispatch({
			type: isSaved ? rootTypes.unSavePost : rootTypes.savePost,
			postId: id
		})

	return (
		<Box className={classNames}>
			<IconButton
				className={classes.icon}
				onClick={handleSave}
			>
				<MaybeSavedIcon isSaved={isSaved} />
			</IconButton>

			<Link
				className={classes.link}
				component={RouterLink}
				color="inherit"
				to={`/post/${id}`}
				variant="subtitle1"
				noWrap
			>
				{title}
			</Link>

			<IconButton className={classes.icon}>
				<Share />
			</IconButton>
		</Box>
	)
}
TileHeader.whyDidYouRender = true

export default TileHeader