import { makeStyles } from '@material-ui/core/styles'

export default makeStyles(({
	breakpoints,
	transitions
}) => ({
	tileBackground: ({ color }) => ({ backgroundColor: color }),
	tile: {
		overflow: 'hidden',
		position: 'relative',
		color: 'white',
		'&:hover': {
			'& $visible': {
				visibility: 'visible'
			},
			'& $image': {
				transform: `scale(1.05)`
			}
		}
	},
	visible: {
		[breakpoints.up('sm')]: {
			visibility: 'hidden'
		}
	},
	image: {
		objectFit: 'cover',
		width: '100%',
		height: '100%',
		display: 'block',
		transition: transitions.create('transform', {
			easing: transitions.easing.easeInOut,
			duration: transitions.duration.short,
		}),
	},
}))
