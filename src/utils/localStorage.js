function getLocalStorageItem(key, initialValue) {
	try {
		// Get from local storage by key
		const item = window.localStorage.getItem(key)
		// Parse stored json or if none return initialValue
		return item ? JSON.parse(item) : initialValue
	} catch (error) {
		// If error also return initialValue
		console.log(error)
		return initialValue
	}
}

function setLocalStorageItem(key, value) {
	window.localStorage.setItem(key, JSON.stringify(value))
	try {
		// Convert the value to a JSON string
		const item = JSON.stringify(value)
		// Save to local storage
		window.localStorage.setItem(key, item)
	} catch (error) {
		console.log(error)
	}
}

export { getLocalStorageItem, setLocalStorageItem }
