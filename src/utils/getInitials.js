const isLetter = char => char.toUpperCase() !== char.toLowerCase()

function getInitials(name) {
	let result = name[0]

	for (let i = 1; i < name.length - 1; i++) {
		const current = name[i]
		const next = name[i + 1]

		if (current === ' ' && isLetter(next)) {
			result += next
			break
		}
	}

	return result
}

export { getInitials }
