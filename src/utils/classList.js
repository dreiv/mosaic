import { compact } from './array';

function classList(...classes) {
	return compact(classes).join(' ')
}

export { classList }
