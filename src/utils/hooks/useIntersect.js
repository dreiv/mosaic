import { useEffect, useRef, useState } from 'react'

export function useIntersect() {
	const [intersects, setIntersects] = useState(false)
	const [node, setNode] = useState(null)

	const observer = useRef(
		new window.IntersectionObserver(([{ intersectionRatio }]) =>
			setIntersects(!!intersectionRatio)
		)
	)

	useEffect(() => {
		const { current } = observer
		current.disconnect()

		if (node) current.observe(node)

		return () => current.disconnect()
	}, [node])

	return [setNode, intersects]
}
